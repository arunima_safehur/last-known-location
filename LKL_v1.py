# coding=utf-8
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
# i#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import Libraries
import datetime
import dateutil.parser
from datetime import datetime
from dateutil import tz
import pytz
import pandas as pd
import numpy as np
from pymongo import MongoClient
from datetime import timedelta
import pygsheets
from config import *
from math import radians, cos, sin, asin, sqrt
pd.options.mode.chained_assignment = None
from bson.son import SON

# Set Display option if needed
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.options.display.max_colwidth = 500

print "datetime naive:", datetime.now()
print "datetime utc:", datetime.utcnow()

gc = pygsheets.authorize(
    service_file='C:/Users/Safehur/Desktop/Arunima/Last Location Data/pythonProject/pygsheets-b7619d00ce02.json')
# Open # user-vehicle-device-organization mappings spreadsheet from googlesheet
sheet = gc.open('uvdo_current_clients')
worksheet = sheet.worksheet_by_title('uvdo')
mappings = worksheet.get_as_df()
print mappings

# user-vehicle-device-organization mappings
imei_list = []
list = mappings["IMEI"].apply(str)

for i in list:
    imei_list.append(i)

print imei_list

# Connect to MongoDB remotely to extract data
myclient = MongoClient('mongodb://pritish:new_life@54.68.56.196/safehur_production')
database = myclient["safehur_production"]
collection_trip_readings = database["trip_readings"]
collection_trip_resync_readings = database["trip_resync_readings"]
print collection_trip_readings.count()
print collection_trip_resync_readings.count()

date = datetime.utcnow()
date = date.replace(hour=0, minute=0, second=0, microsecond=0)
date = date - timedelta(hours=5, minutes=30)
date = date - timedelta(days=3)

print "date: ", date
new_date = date + timedelta(days=4)
print "new_date: ", new_date

# query for getting locked data from the server
trip_reading_query_for_LKL = {"$and": [{"device_id": {"$in": imei_list}}, {"created_at": {"$gte": date, "$lt": new_date}
                                                                           }, {"reading_data": {"$regex": ",A,"}}]}
trip_reading_raw_for_LKL = collection_trip_readings.find(trip_reading_query_for_LKL).sort("created_at", -1)

print "trip reading raw pkts: ", trip_reading_raw_for_LKL.count()
trip_reading_data_for_LKL = []

for i in trip_reading_raw_for_LKL:
    trip_reading_data_for_LKL.append(i)

trip_readings_for_LKL = pd.DataFrame(trip_reading_data_for_LKL)
print (len(trip_readings_for_LKL))

trip_readings_for_LKL['created_at'] = pd.to_datetime(trip_readings_for_LKL.created_at, errors='coerce')
trip_readings_for_LKL['created_at'] = trip_readings_for_LKL['created_at'].dt.strftime('%Y-%m-%d %H:%M:%S')


def string_to_datetime(z):
    try:
        dt = datetime.strptime(z, '%Y-%m-%d %H:%M:%S')
        return dt
    except:
        pass


trip_readings_for_LKL['created_at'] = map(string_to_datetime, trip_readings_for_LKL['created_at'])
trip_readings_for_LKL['created_at'] = trip_readings_for_LKL.created_at + pd.Timedelta('05:30:00')

trip_readings_for_LKL = trip_readings_for_LKL[['device_id', 'created_at', 'reading_data']]
trip_readings_for_LKL = trip_readings_for_LKL.drop_duplicates('created_at')
a = trip_readings_for_LKL[
    trip_readings_for_LKL.groupby('device_id')['created_at'].transform('max') == trip_readings_for_LKL['created_at']]


# helper function for GPRMC check
def GPRMC_checker(x):
    if "$GPRMC" in x:
        result = x
    else:
        result = "Not available"
    return result


a['reading_data'] = map(GPRMC_checker, a['reading_data'])
a = a[~a.reading_data.str.contains("Not available")]

print a.count()

newdf = a
newdf['gps_string'] = newdf['reading_data'].apply(lambda s: s.split(';')[3])
newdf['lat'] = pd.to_numeric(newdf['gps_string'].apply(lambda s: s.split('$GPRMC')[1].split(',')[3]))
newdf['long'] = pd.to_numeric(newdf['gps_string'].apply(lambda s: s.split('$GPRMC')[1].split(',')[5]))


# Helper function for lat-long converter
def dm_lat(x_lat):
    degrees_x = int(x_lat) // 100
    minutes_x = x_lat - 100 * degrees_x
    lat_x = degrees_x + minutes_x / 60

    return lat_x


def dm_long(y_long):
    degrees_y = int(y_long) // 100
    minutes_y = y_long - 100 * degrees_y
    long_y = degrees_y + minutes_y / 60

    return long_y


newdf['Latitude'] = map(dm_lat, newdf['lat'])
newdf['Longitude'] = map(dm_long, newdf['long'])

newdf = newdf.rename(columns={'device_id': 'IMEI'})
newdf['IMEI'] = pd.to_numeric(newdf['IMEI'])
newdf['date'] = newdf['created_at'].dt.date
newdf['time'] = newdf['created_at'].dt.time
newdf = newdf[['IMEI', 'created_at', 'date', 'time', 'Latitude', 'Longitude']]
# newdf.tail()

newdf = pd.merge(mappings[["Organization Name", 'Organization ID', "IMEI", "Vehicle Number", "Driver Name"]],
                 newdf[[u'IMEI', u'created_at', u'date', u'time', u'Latitude', u'Longitude', ]], on=["IMEI"],
                 how='inner')

# reading data from config file
dist = location_details_dict

len_dist = (len(dist) + sum(len(v) for v in dist.itervalues())) / 6
print len_dist


def haversine(lon1, lat1, lon2, lat2):
    """
        Calculate the great circle distance between two points
        on the earth (specified in decimal degrees)
        """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return c * r


def location_finder(x, y, z):
    test_point = [{'lat': x, 'lng': y}]

    # 19.077691	73.075356
    for i in range(1, len_dist + 1):
        i = str(i)
        lat1 = dist[i]['lat']
        lon1 = dist[i]['lng']
        location = dist[i]['area']
        radius = dist[i]['radius']
        o_id = dist[i]['o_id']
        lat2 = test_point[0]['lat']
        lon2 = test_point[0]['lng']
        a_distance = haversine(lon1, lat1, lon2, lat2)
        if a_distance <= radius and o_id == z:

            break
        else:
            location = "Outside Area !"

    return location


newdf['Last Known Location'] = map(location_finder, newdf['Latitude'], newdf['Longitude'], newdf['Organization ID'])
newdf.rename(columns={'created_at': 'Created timestamp '}, inplace=True)
# newdf['Lat-Long'] = newdf['latitude'].map(str) + " - " + newdf['longitude'].map(str)
newdf = newdf[[u'Organization Name', u'Organization ID', u'IMEI',
               u'Vehicle Number', u'Driver Name', u'Created timestamp ', u'Latitude',
               u'Longitude', u'Last Known Location']]

newdf.to_csv("LKL_result_12MAR21_test.csv")

print newdf
