import pandas as pd


def one_hour_time_band(tme):
    global time_band_one_hour
    if tme >= "00:00:00" and tme < "01:00:00":
        time_band_one_hour = "00:00-01:00"
    elif tme >= "01:00:00" and tme < "02:00:00":
        time_band_one_hour = "01:00-02:00"
    elif tme >= "02:00:00" and tme < "03:00:00":
        time_band_one_hour = "02:00-03:00"
    elif tme >= "03:00:00" and tme < "04:00:00":
        time_band_one_hour = "03:00-04:00"
    elif tme >= "04:00:00" and tme < "05:00:00":
        time_band_one_hour = "04:00-05:00"
    elif tme >= "05:00:00" and tme < "06:00:00":
        time_band_one_hour = "05:00-06:00"
    elif tme >= "06:00:00" and tme < "07:00:00":
        time_band_one_hour = "06:00-07:00"
    elif tme >= "07:00:00" and tme < "08:00:00":
        time_band_one_hour = "07:00-08:00"
    elif tme >= "08:00:00" and tme < "09:00:00":
        time_band_one_hour = "08:00-09:00"
    elif tme >= "09:00:00" and tme < "10:00:00":
        time_band_one_hour = "09:00-10:00"
    elif tme >= "10:00:00" and tme < "11:00:00":
        time_band_one_hour = "10:00-11:00"
    elif tme >= "11:00:00" and tme < "12:00:00":
        time_band_one_hour = "11:00-12:00"
    elif tme >= "12:00:00" and tme < "13:00:00":
        time_band_one_hour = "12:00-13:00"
    elif tme >= "13:00:00" and tme < "14:00:00":
        time_band_one_hour = "13:00-14:00"
    elif tme >= "14:00:00" and tme < "15:00:00":
        time_band_one_hour = "14:00-15:00"
    elif tme >= "15:00:00" and tme < "16:00:00":
        time_band_one_hour = "15:00-16:00"
    elif tme >= "16:00:00" and tme < "17:00:00":
        time_band_one_hour = "16:00-17:00"
    elif tme >= "17:00:00" and tme < "18:00:00":
        time_band_one_hour = "17:00-18:00"
    elif tme >= "18:00:00" and tme < "19:00:00":
        time_band_one_hour = "18:00-19:00"
    elif tme >= "19:00:00" and tme < "20:00:00":
        time_band_one_hour = "19:00-20:00"
    elif tme >= "20:00:00" and tme < "21:00:00":
        time_band_one_hour = "20:00-21:00"
    elif tme >= "21:00:00" and tme < "22:00:00":
        time_band_one_hour = "21:00-22:00"
    elif tme >= "22:00:00" and tme < "23:00:00":
        time_band_one_hour = "22:00-23:00"
    elif tme >= "23:00:00" and tme < "00:00:00":
        time_band_one_hour = "23:00-00:00"
    return time_band_one_hour


def speed_band(spd):
    global spd_bnd
    if spd > 0 and spd <= 10:
        spd_bnd = "0-10"
    elif spd > 10 and spd <= 20:
        spd_bnd = "10-20"
    elif spd > 20 and spd <= 30:
        spd_bnd = "20-30"
    elif spd > 30 and spd <= 40:
        spd_bnd = "30-40"
    elif spd > 40 and spd <= 50:
        spd_bnd = "40-50"
    elif spd > 50 and spd <= 60:
        spd_bnd = "50-60"
    elif spd > 60 and spd <= 70:
        spd_bnd = "60-70"
    elif spd > 70 and spd <= 80:
        spd_bnd = "70-80"
    elif spd > 80 and spd <= 90:
        spd_bnd = "80-90"
    elif spd > 90 and spd <= 100:
        spd_bnd = "90-100"
    elif spd > 100 and spd <= 110:
        spd_bnd = "100-110"
    elif spd > 110 and spd <= 120:
        spd_bnd = "110-120"
    elif spd > 120 and spd <= 130:
        spd_bnd = "120-130"
    elif spd > 130 and spd <= 140:
        spd_bnd = "130-140"
    elif spd > 140 and spd <= 150:
        spd_bnd = "140-150"
    return spd_bnd