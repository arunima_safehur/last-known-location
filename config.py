# coding=utf-8
# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.


location_details_dict = {
        '1': {'lat': 18.9596182, 'lng': 73.0404031, 'area': "Jasai Parking Area !", "radius": 5, 'o_id': 22},

        '2': {'lat': 18.7491393002317, 'lng': 73.79143379915689, 'area': "Pune Parking Chakan Area !", "radius": 5,
              'o_id': 22},

        '3': {'lat': 18.90943135046065, 'lng': 73.04696680958459, 'area': "New Jasai Parking Area !", "radius": 5,
              'o_id': 22},

        '4': {'lat': 28.21077888, 'lng': 76.79339008, 'area': "Dharuhera Area !", "radius": 5, 'o_id': 30},

        '5': {'lat': 28.5318129, 'lng': 77.4564936, 'area': "Covestro India Noida Area !", "radius": 5, 'o_id': 30},

        '6': {'lat': 28.284305796731665, 'lng': 77.30534071258937, 'area': "Amazon Saras compound Area !", "radius": 3,
              'o_id': 35},

        '7': {'lat': 28.370133578642037, 'lng': 76.87824723463122, 'area': "Amazon NCRU Panchgaon Area !", "radius": 4,
              'o_id': 35},

        '8': {'lat': 28.392944376873597, 'lng': 76.8952159043533, 'area': "Amazon DELU Area !", "radius": 4,
              'o_id': 35},

        '9': {'lat': 29.0839883771194, 'lng': 77.1572941082005, 'area': "Amazon DELV Area !", "radius": 4, 'o_id': 35},

        '10': {'lat': 28.34175831342738, 'lng': 77.32181483205986, 'area': "Balabhgarh Area !", "radius": 10,
               'o_id': 35},
        '11': {'lat':28.2336324, 'lng': 76.8153305, 'area': "Dharuhera TCI-IOCL Area !", "radius": 4, 'o_id': 4}

        }

